package com.epam;

/**
 * @author Gabchak Roman
 * @version 1.0
 */

/**
 * Main program class.
 */

public final class Main {
    /**
     * Starting point.
     * @param args required string array
     */
    public static void main(final String[] args) {
        System.out.println("Even and odd numbers");
        EvenAndOddNumbers evenAndOddNumbers = new EvenAndOddNumbers();
        evenAndOddNumbers.run();
        int fibNum1 = evenAndOddNumbers.getMaxOddNumber();
        int fibNum2 = evenAndOddNumbers.getMaxEvenNumber();

        System.out.println("\nFibonacci numbers");
        FibonacciNumbers fibNum = new FibonacciNumbers(fibNum1, fibNum2);
        fibNum.run();
    }
}
