package com.epam;

import java.util.Scanner;

/**
 * * Class even and odd numbers.
 */
public class EvenAndOddNumbers {
    /**
     * Interval form.
     */
    private int fromInputNumber;
    /**
     * Interval to.
     */
    private int toInputNumber;
    /**
     * Sum of odd numbers.
     */
    private int sumOfOddNumbers;
    /**
     * Sum of even numbers.
     */
    private int sumOfEvenNumbers;
    /**
     * Max odd number.
     */
    private int maxOddNumber;
    /**
     * Min odd number.
     */
    private int maxEvenNumber;

    /**
     * Сhecks if the entered data is a number.
     * @return input number.
     */
    private int inputCheck() {
        Scanner scanner = new Scanner(System.in);
        while (!scanner.hasNextInt()) {
            System.out.println("This is not a number!");
            scanner.next();
        }
        return scanner.nextInt();
    }

    /**
     * Take input numbers from the keyboard.
     */
    private void getInputNumbers() {
        System.out.println("Enter interval");
        System.out.println("first number: ");
        fromInputNumber = inputCheck();
        System.out.println("second number: ");
        toInputNumber = inputCheck();
    }

    /**
     * Count the sum odd and even numbers.
     */
    private void countSumOddAndEvenNumbers() {
        for (int i = fromInputNumber + 1; i <= toInputNumber; i++) {
            if (i % 2 == 0) {
                sumOfEvenNumbers += i;
            } else {
                sumOfOddNumbers += i;
            }
        }
    }

    /**
     * Set the maximal odd and even number.
     */
    public final void setMaxOddAndEvenNumber() {
        if ((Math.abs(toInputNumber - fromInputNumber) >= 2)
                && (toInputNumber % 2) == 0) {
            maxEvenNumber = toInputNumber;
            maxOddNumber = toInputNumber - 1;
        } else {
            maxOddNumber = toInputNumber;
            maxEvenNumber = toInputNumber - 1;
        }
    }

    /**
     * Displays the sum odd and even numbers.
     */
    private void printSumOddAndEvenNumbers() {
        countSumOddAndEvenNumbers();
        System.out.println("\nSum of odd numbers: " + sumOfOddNumbers);
        System.out.println("Sum of even numbers: " + sumOfEvenNumbers);
    }

    /**
     * Displays odd numbers.
     */
    private void printEvenNumbers() {
        System.out.print("\nEven number output in descending order: \n");
        for (int i = toInputNumber; i >= fromInputNumber; i--) {
            if (i % 2 == 0) {
                System.out.print("[" + i + "] ");
            }
        }
    }

    /**
     * Displays odd numbers.
     */
    private void printOddNumbers() {
        System.out.print("\nOdd numbers output in ascending order: \n");
        for (int i = fromInputNumber; i <= toInputNumber; i++) {
            if (i % 2 != 0) {
                System.out.print("[" + i + "] ");
            }
        }
    }

    /**
     * Return max odd number.
     * @return Max odd number.
     */
     final int getMaxOddNumber() {
        return maxOddNumber;
    }
    /**
     * Return max even number.
     * @return Max even number.
     */
     final int getMaxEvenNumber() {
        return maxEvenNumber;
    }

    /**
     * Starts the program.
     */
    public final void run() {
        getInputNumbers();
        setMaxOddAndEvenNumber();
        printOddNumbers();
        printEvenNumbers();
        printSumOddAndEvenNumbers();
    }

}
