package com.epam;

import java.util.Scanner;

/**
 * * Class describes Fibonacci sequence.
 */
final class FibonacciNumbers {
    /**
     * Odd numbers sum.
     */
    private int sumOddNumbers;
    /**
     * Even numbers sum.
     */
    private int sumEvenNumbers;
    /**
     * fibonacci F1.
     */
    private int fibonacciN1;
    /**
     * fibonacci F2.
     */
    private int fibonacciN2;

    /**
     * Constructor fibonacci numbers F1 and F2.
     *
     */
    FibonacciNumbers(final int fibonacciN1, final int fibonacciN2) {
        this.fibonacciN1 = fibonacciN1;
        this.fibonacciN2 = fibonacciN2;
    }

    /**
     * Сhecks if the entered data is a number.
     */
    private int inputNumber() {
        Scanner scanner = new Scanner(System.in);
        while (!scanner.hasNextInt()) {
            System.out.println("This is not a number!");
            scanner.next();
        }
        return scanner.nextInt();
    }

    /**
     * Displays the Fibonacci numbers and count the sum Odd and Even numbers.
     */
    private void printFibonacciNumber() {
        System.out.println("Enter the number: ");
        int numberIn = inputNumber();
        if (numberIn <= 0) {
            System.out.println(0);
        }

        int number0 = fibonacciN1;
        int number1 = fibonacciN2;
        int sum;
        for (int i = 0; i < numberIn; i++) {
            if (i == 0) {
                sum = number0;
            } else if (i == 1) {
                sum = number1;
            } else {
                sum = number0 + number1;
                number0 = number1;
                number1 = sum;
            }
            if (sum % 2 == 0) {
                sumEvenNumbers += sum;
            } else {
                sumOddNumbers += sum;
            }
            System.out.print(sum + " ");
        }
        System.out.println();
    }

    /**
     * Count and displays the percentage of odd
     * and even Fibonacci numbers on the screen.
     */
    private void printPercentageOfOddAndEvenNumbers() {
        float total = sumOddNumbers + sumEvenNumbers;
        final float PERCENT = 100;

        float percentOfOddNumbers = (sumOddNumbers / total) * PERCENT;
        float percentOfEvenNumbers = (sumEvenNumbers / total) * PERCENT;

        System.out.println("Odd numbers: " + percentOfOddNumbers + "%");
        System.out.println("Even numbers: " + percentOfEvenNumbers + "%");
    }

    /**
     * Starts the program.
     */
    public void run() {
        printFibonacciNumber();
        printPercentageOfOddAndEvenNumbers();
    }
}
